def hello_world():
    'It is a function to print hello world '
    
    return ("Hello How are you?")

def sq(a):
    return a**2

print(hello_world().upper())
print(hello_world.__doc__)

print(len.__doc__)

#Check predefined functions
st = "hii"
print(dir(st))

print("List:")
print(dir([]))

print("Tuple:")
print(dir(()))