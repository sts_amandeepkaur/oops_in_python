class Student:
    def __init__(self,name,marks):
        self.n = name
        self.m = marks
        print("Values Initialized")

    def hello(self):
        print("Name:{} Marks:{}\n".format(self.n,self.m))

    def __del__(self):
        del self.n
        del self.m
        print("Destructor called")

st1 = Student("Aman",78)
st2 = Student("Peter",80)
st1.hello()
st2.hello()

if st1.m > st2.m:
    print(st1.n, "has highest marks")
else:
    print(st2.n,"has highest marks")