class Student:
    college = "XYZ Engineering College"
    is_book_issued = False

    def intro(self,name,age):
        self.n = name
        print("Name: {} Age: {} College: {}".format(name,age
        ,self.college))

    def library(self,book_name):
        self.is_book_issued = True
        print("Book: {} issued by: {}".format(book_name,self.n))

#Creating Object
st1 = Student()
st2 = Student()

st1.intro("John",20)
st2.intro("Peter",22)
st1.library("Python Programming")
print("BOOK ISSUED:", st1.is_book_issued)
print("BOOK ISSUED:", st2.is_book_issued)
#Accessing values
print(st1.college)

